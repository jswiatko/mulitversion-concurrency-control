import java.util.*;


// IMPORTANT -- THIS IS INDIVIDUAL WORK. ABSOLUTELY NO COLLABORATION!!!


// implement a (main-memory) data store with MVCC.
// objects are <int, int> key-value pairs.
// if an operation is to be refused by the MVCC protocol,
// undo its xact (what work does this take?) and throw an exception.
// garbage collection of versions is optional.
// Throw exceptions when necessary, such as when we try to execute an operation in
// a transaction that is not running; when we insert an object with an existing
// key; when we try to read or write a nonexisting key, etc.
// You may but do not need to create different exceptions for operations that
// are refused and for operations that are refused and cause the Xact to be
// aborted. Keep it simple!
// Keep the interface, we want to test automatically!

public class MVCC {
    static class Version {
        public int versionNr;
        public int value;
        Version(){
            versionNr = 0;
            value = 0;
        }
        Version(int versionNr,int value) {
            this.versionNr = versionNr;
            this.value = value;
        }

    }
    static class ObjectInfo {
        public int rts;
        public List<Version> versions;
        ObjectInfo(int versionNr, int value) {
            rts = 0;
            versions = new ArrayList<Version>();
            versions.add(new Version(versionNr,value));
        }
    }

    static class XactLog {
        String status; // uncommitted, aborted, waiting, committed
        public List<Integer> dependsOn;
        // Xact has to know which other xacts depend on it to notify them them it is committed or aborted
        public List<Integer> dependants;
        public List<Integer> queries;
        XactLog() {
            dependsOn = new ArrayList<Integer>();
            dependants = new ArrayList<Integer>();
            queries = new ArrayList<Integer>();
            status = "uncommitted";
        }
    }

    // Maybe this can be store in an array
    private static Map<Integer,ObjectInfo> store =
            new HashMap<Integer,ObjectInfo>();
    private static Map<Integer,XactLog> xacts =
            new HashMap<Integer,XactLog>();
    private static int max_xact = 0;

    // returns transaction id == logical start timestamp
    public static int begin_transaction() {
        ++max_xact;
        xacts.put(max_xact,new XactLog());
        return max_xact;
    }

    // create and initialize new object in transaction xact
    public static void insert(int xact, int key, int value) throws Exception
    {
        // We can assume that inserts happen in only first transaction
        // Check if transaction is alive, otherwise throw an excepiton
        if(!xacts.containsKey(xact) || xacts.get(xact).status.equals("aborted")) {
            throw new Exception("Cannot insert key " + key + " in xact " + xact + " because this transaction does not exist");
        }

        if(!xacts.get(xact).status.equals("uncommitted")) {
            rollback(xact);
            throw new Exception("Cannot insert object " + key + " because transaction " + xact + " has already been or is being committed.");
        }

        // If key already exists throw an exception
        if(store.containsKey(key)) {
            rollback(xact);
            throw new Exception("Cannot insert key " + key + " in xact " + xact + " because it is already inserted");
        }

        // Test for modqueries

        for(int queryXact = max_xact;queryXact > xact;--queryXact) {
            for(Integer k : xacts.get(queryXact).queries) {
                if((value % k) == 0) {
                    rollback(xact);
                    //ROLLBACK T(2):I(3,18)
                    throw new Exception("ROLLBACK T(" + xact + "):I("+ key + "," + value + ")");
                }
            }
        }
        store.put(key,new ObjectInfo(xact,value));
    }

    // return value of object key in transaction xact
    public static int read(int xact, int key) throws Exception
    {
        // Check if transaction is alive, otherwise throw an exception

        if(!xacts.containsKey(xact) || xacts.get(xact).status.equals("aborted")) {
            throw new Exception("Cannot read object " + key + " because transaction " + xact + " does not exist");
        }

        if(!xacts.get(xact).status.equals("uncommitted")) {
            rollback(xact);
            throw new Exception("Cannot read object " + key + " because transaction " + xact + " has already been or is being committed.");
        }

        if((!store.containsKey(key))) {
            rollback(xact);
            throw new Exception("Cannot read object " + key + " because it does not exist");
        }



        int versionsSize = store.get(key).versions.size();
        ListIterator<Version> iterator = store.get(key).versions.listIterator(versionsSize);
        Version ver = null;
        // find most recent version that is not older then xact
        boolean isVersionFound = false;
        while (iterator.hasPrevious()) {
            ver = iterator.previous();
            if(ver.versionNr <= xact && !xacts.get(ver.versionNr).status.equals("aborted") ) {
                isVersionFound = true;
                break;
            }
        }
        // If we did not assume that objects are injected only at the beginning we would have to check
        if(isVersionFound == false) {
             rollback(xact);
             throw new Exception("Cannot read object " + key + " because it does not exist (it was inserted after transaction " + xact + ")");
        }
        // Set RTS
        if(store.get(key).rts < xact)
            store.get(key).rts = xact;

        // If read uncommitted version:
        // - add this action to the list of transactions that read this uncommitted
        //   version in the list of that object version
        // - add this action to the history of actions of this transaction with the
        //   object that was read and the index of version that was read in the list of this objects versions

        // If xact that wrote this version is in status uncommitted
        // or waiting add it to dependsOn of current xact.
        // Additionally add it to dependants of uncommitted/waiting xact.
        if(ver.versionNr != xact
                && !xacts.get(ver.versionNr).status.equals("committed")
                && !xacts.get(xact).dependsOn.contains(ver.versionNr)) {
            xacts.get(xact).dependsOn.add(ver.versionNr);
            xacts.get(ver.versionNr).dependants.add(xact);
        }
        return ver.value;
    }

    // write value of existing object identified by key in transaction xact
    public static void write(int xact, int key, int value) throws Exception
    {
        // Check if transaction is alive, otherwise throw an exception
        if(!xacts.containsKey(xact) || !xacts.get(xact).status.equals("uncommitted")) {
            rollback(xact);
            throw new Exception("Cannot write object " + key + " because transaction " + xact + " does not exist");

        }

        if(store.get(key).rts <= xact) {
            // If last version is of the same xact, just update value
            Version ver = store.get(key).versions.get(store.get(key).versions.size() - 1);
            if(ver.versionNr == xact)
                ver.value = value;
            else
                // Else add new version
                store.get(key).versions.add(new Version(xact,value));
        }
        else {
            rollback(xact);
            throw new Exception("ROLLBACK T(" + xact + "):W("+ key + "," + value + ")");
        }
    }

    // Implementing queries is OPTIONAL for bonus points!
    // return the list of keys of objects whose values mod k are zero.
    // this is our only kind of query / bulk read. Your implementation must still
    // guarantee serializability. How do you deal with inserts? By maintaining
    // a history of querys with suitable metadata (xact?). Do you need a form of
    // locking?
    public static List<Integer> modquery(int xact, int k) throws Exception
    {
        List<Integer> l = new ArrayList<Integer>();
        if(!xacts.containsKey(xact) || xacts.get(xact).status.equals("aborted")) {
            throw new Exception("Cannot bulk MODQUERY because transaction " + xact + " does not exist");
        }

        if(!xacts.get(xact).status.equals("uncommitted")) {
            rollback(xact);
            throw new Exception("Cannot MODQUERY because transaction " + xact + " has already been or is being committed.");
        }

        xacts.get(xact).queries.add(k);
        for(Integer key : store.keySet()) {
            int versionsSize = store.get(key).versions.size();
            ListIterator<Version> iterator = store.get(key).versions.listIterator(versionsSize);
            Version ver = null;
            // find most recent version that is not older then xact
            boolean isVersionFound = false;
            while (iterator.hasPrevious()) {
                ver = iterator.previous();

                if(ver.versionNr <= xact && !xacts.get(ver.versionNr).status.equals("aborted") ) {
                    isVersionFound = true;
                    break;
                }
            }
            // If we did not assume that objects are injected only at the beginning we would have to check
            if(isVersionFound == false) {
                rollback(xact);
                throw new Exception("Cannot read object " + key + " because it does not exist (it was inserted after transaction " + xact + ")");
            }
            // Set RTS
            if(ver.value % k == 0) {
                if(store.get(key).rts < xact)
                    store.get(key).rts = xact;

                if(ver.versionNr != xact
                        && !xacts.get(ver.versionNr).status.equals("committed")
                        && !xacts.get(xact).dependsOn.contains(ver.versionNr)) {
                    xacts.get(xact).dependsOn.add(ver.versionNr);
                    xacts.get(ver.versionNr).dependants.add(xact);
                }
                l.add(key);
            }
        }

        return l;
    }

    public static void commit(int xact)   throws Exception {

        // To prevent multiple commits.
        if(xacts.get(xact).status.equals("committed"))
            return;
        if(!xacts.containsKey(xact) || xacts.get(xact).status.equals("aborted")) {
            throw new Exception("T(" + xact + ") DOES NOT EXIST");
        }
        boolean waitFlag = false;
        for(Integer depsOnXact : xacts.get(xact).dependsOn) {
            if(xacts.get(depsOnXact).status.equals("committed"))
                continue;
            //
            else if(xacts.get(depsOnXact).status.equals("aborted")) {
                rollback(xact);
            }
            else { // status equals uncommitted or waiting
                xacts.get(xact).status = "waiting";
                waitFlag = true;
            }
        }
        // if there was at list one blocking xact then return
        if(waitFlag)
            return;
        // if here then commit succesfull set status to committed
        // notify all the threads/xact in BlockedXacts list
        xacts.get(xact).status = "committed";
        for(Integer depXact : xacts.get(xact).dependants) {
            if(xacts.get(depXact).status.equals("waiting"))
                commit(depXact);
        }
    }
    public static void rollback(int xact) throws Exception {
        // Check if already not rollbacked
        if(xacts.get(xact).status.equals("aborted")) {
            return;
        }
        if(!xacts.containsKey(xact)) {
            throw new Exception("Cannot rollback xact " + xact + " because it does not exist.");
        }

        xacts.get(xact).status = "aborted";
        for(Integer depXact : xacts.get(xact).dependants) {
            // Abort all uncommitted and waiting dependant xacts
            if(!xacts.get(depXact).status.equals("aborted"))
                rollback(depXact);
        }
    }
}


